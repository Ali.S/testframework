﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace YahtzeeTester
{
    class ScoreOnesTest : GenericScoreTest
    {
        [DontRunAfter(typeof(GenericScoreTest))]
        [RunAfterInstance(typeof(RollTest), "(i)=>i", typeof(GenericScoreTest))]
        public ScoreOnesTest(StartTest starter, RollResult previousRoll) : base(starter, previousRoll, 0)
        {
        }

        [RunAfterInstance(typeof(RollTest), "(i)=>i", typeof(GenericScoreTest))]
        public ScoreOnesTest(StartTest starter, RollResult previousRoll, GenericScoreTest PreviousTest) : base(starter, previousRoll, PreviousTest.ReportedScore)
        {
        }

        public void runTest(Session session)
        {
            int reportedScore = markScore("Sum_1");
            if (reportedScore != PreviousRoll.Roll.Where(d => d == 1).Sum() + PreviousScore)
                throw new Exception("Wrong reported result");
        }
    }
}
