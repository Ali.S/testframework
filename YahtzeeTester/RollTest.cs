﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace YahtzeeTester
{
    class RollTest : RollResult
    {

        [DontRunAfter(typeof(RollTest))]
        public RollTest(StartTest starter) : base(starter)
        {
        }

        
        public RollTest(
            StartTest starter,
            [InstanceRequirement("(i) => i-1")]
            GenericScoreTest dummy) : base(starter)
        {
        }

        public void runTest(Session session)
        {
            readRolls();
        }
    }
}
