﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace YahtzeeTester
{
    class Program
    {
        static void Main(string[] args)
        {
            AppBridge server = new AppBridge("../../Yahtzee.exe");
            string line = server.Output.ReadLine();
            server.Input.WriteLine("S");
            TestItem[][] defs = new TestItem[][]
            {
                new TestItem[]
                {
                    new TestItem(typeof(ScoreOnesTest)),
                    new TestItem(typeof(ScoreTwosTest))
                },
                new TestItem[]
                {
                    new TestItem(typeof(RerollTest)),
                    new TestItem(typeof(ScoreOnesTest))
                },
                new TestItem[]
                {
                    new TestItem(typeof(RollTest)),
                    new TestItem(typeof(RerollTest)),
                    new TestItem(typeof(RerollTest)),
                    new TestItem(typeof(ScoreOnesTest)),
                    new TestItem(typeof(RollTest)),
                    new TestItem(typeof(RerollTest)),
                    new TestItem(typeof(RerollTest)),
                    new TestItem(typeof(ScoreTwosTest))
                },
                new TestItem[]
                {
                    new TestItem(typeof(ScoreOnesTest)),
                    new TestItem(typeof(ScoreTwosTest)),
                    new TestItem(typeof(ScoreThreesTest)),
                    new TestItem(typeof(ScoreFoursTest)),
                    new TestItem(typeof(ScoreFivesTest)),
                    new TestItem(typeof(ScoreSixesTest))
                }
            };

            for (var i = 0; i < defs.Length; i++)
            {
                Scenario scenario = new Scenario(defs[i]);
                var roadMap = scenario.CreateRoadMap();
                Console.WriteLine(string.Join(" => ", roadMap.Tests.Select(t => t.TestType)));

                Dictionary<Type, double> runTimes = new Dictionary<Type, double>();
                foreach (var type in roadMap.Tests.Select(c => c.TestType).Distinct())
                    runTimes.Add(type, 0);

                bool AllTestsPassed = true;
                int repeat = 100;
                for (int k = 0; k < repeat; k++)
                {
                    var session = roadMap.createSessionWithRandomParameters();
                    var run = session.Run();
                    run.Wait();
                    if (!run.IsFaulted)
                    {
                        foreach (var item in session.getRunTimes())
                            runTimes[item.Key] += item.Value;
                    }
                    else
                    {
                        AllTestsPassed = false;
                        Console.WriteLine(run.Exception.ToString());
                        break;
                    }
                }

                Console.WriteLine("Test results #{0}", i);

                if (AllTestsPassed)
                {

                    foreach (var item in runTimes)
                        Console.WriteLine("\t {0}: {1:0.00}", item.Key, item.Value / repeat);
                }
            }

            server.Dispose();
        }
    }
}
