﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TestFramework;

namespace YahtzeeTester
{
    abstract class RollResult
    {
        public StartTest Starter;
        public int[] Roll { get; private set; }
        protected RollResult(StartTest starter) {
            Roll = new int[5];
            Starter = starter;
        }
        protected void readRolls()
        {
            string line = Starter.Client.Output.ReadLine();
            string format = "Your roll is: ([1-6]), ([1-6]), ([1-6]), ([1-6]), ([1-6])";
            var match = Regex.Match(line, format);
            Roll[0] = int.Parse(match.Groups[1].Value);
            Roll[1] = int.Parse(match.Groups[2].Value);
            Roll[2] = int.Parse(match.Groups[3].Value);
            Roll[3] = int.Parse(match.Groups[4].Value);
            Roll[4] = int.Parse(match.Groups[5].Value);
        }
    }
}
