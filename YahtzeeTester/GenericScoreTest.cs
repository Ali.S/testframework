﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TestFramework;

namespace YahtzeeTester
{
    class GenericScoreTest
    {
        public RollResult PreviousRoll { get; private set; }
        public StartTest Starter { get; private set; }
        public int ReportedScore { get; private set; }
        public int PreviousScore { get; private set; }
        protected GenericScoreTest(StartTest starter, RollResult previousRoll, int previousScore)
        {
            Starter = starter;
            PreviousRoll = previousRoll;
            PreviousScore = previousScore;
        }

        protected int markScore(string slot)
        {
            RerollTest reroll1 = null, reroll2 = null;
            reroll1 = PreviousRoll as RerollTest;
            if (reroll1 != null)
                reroll2 = reroll1.PreviousRoll as RerollTest;
            if (reroll2 == null)
            {
                Starter.Client.Output.ReadLine();
                Starter.Client.Input.WriteLine("1 1 1 1 1");
                Starter.Client.Output.ReadLine();
            }
            if (reroll1 == null)
            {
                Starter.Client.Output.ReadLine();
                Starter.Client.Input.WriteLine("1 1 1 1 1");
                Starter.Client.Output.ReadLine();
            }
            var line = Starter.Client.Output.ReadLine();
            if (!line.Contains(slot))
                throw new Exception("Selected slot is not expected");
            Starter.Client.Input.WriteLine(slot);
            line = Starter.Client.Output.ReadLine();
            string format = "Total Score = (0|[1-9][0-9]*)";
            var match = Regex.Match(line, format);
            ReportedScore = int.Parse(match.Groups[1].Value);
            return ReportedScore;
        }
    }
}
