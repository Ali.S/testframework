﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace YahtzeeTester
{
    class StartTest
    {
        
        public AppBridge Client { get; private set; }

        public void runTest(Session session)
        {
            Client = new AppBridge("../../Yahtzee.exe");
            string ignore1 = Client.Output.ReadLine();
            Client.Input.WriteLine("c");
            string ignore2 = Client.Output.ReadLine();
            Client.Input.WriteLine("127.0.0.1");
        }
    }
}
