﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace YahtzeeTester
{
    class RerollTest : RollResult
    {
        public RollResult PreviousRoll { get; private set; }
        [RunAfterInstance(typeof(RollResult), "(i)=>i-1", typeof(RollResult))]
        public RerollTest(StartTest starter, RollResult previousRoll) : base(starter)
        {
            PreviousRoll = previousRoll;
        }  

        public void runTest(Session session,
            [TestListRange(ValidArguments = new object[] { true, false })] bool hold1,
            [TestListRange(ValidArguments = new object[] { true, false })] bool hold2,
            [TestListRange(ValidArguments = new object[] { true, false })] bool hold3,
            [TestListRange(ValidArguments = new object[] { true, false })] bool hold4,
            [TestListRange(ValidArguments = new object[] { true, false })] bool hold5
            )
        {
            var line = Starter.Client.Output.ReadLine();
            var str = String.Format("{0} {1} {2} {3} {4}", hold1 ? 1 : 0, hold2 ? 1 : 0, hold3 ? 1 : 0, hold4 ? 1 : 0, hold5 ? 1 : 0);
            Starter.Client.Input.WriteLine(str);
            readRolls();
            if (hold1 && (PreviousRoll.Roll[0] != Roll[0]))
                throw new Exception("hold 1 did not work as expected");
            if (hold2 && (PreviousRoll.Roll[1] != Roll[1]))
                throw new Exception("hold 2 did not work as expected");
            if (hold3 && (PreviousRoll.Roll[2] != Roll[2]))
                throw new Exception("hold 3 did not work as expected");
            if (hold4 && (PreviousRoll.Roll[3] != Roll[3]))
                throw new Exception("hold 4 did not work as expected");
            if (hold5 && (PreviousRoll.Roll[4] != Roll[4]))
                throw new Exception("hold 5 did not work as expected");
        }
    }
}
