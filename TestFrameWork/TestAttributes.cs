﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Scripting;

namespace TestFramework
{
    class ScriptCache
    {
        static public Dictionary<string, Func<int, int>> scripts = new Dictionary<string, Func<int, int>>();
    }
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	public class InstanceRequirementAttribute : Attribute
	{
		public readonly Func<int, int> Instance;
        public readonly Type CountMeAs;
		public InstanceRequirementAttribute(string instance, Type countMeAs = null)
		{
            if (ScriptCache.scripts.ContainsKey(instance))
            {
                Instance = ScriptCache.scripts[instance];
            }
            else
            {
                var funcTask = CSharpScript.EvaluateAsync<Func<int, int>>("new System.Func<int, int>(" + instance + ")");
                funcTask.Wait();
                Instance = funcTask.Result;
                ScriptCache.scripts.Add(instance, Instance);
            }
            CountMeAs = countMeAs;
		}
	}

	[AttributeUsage(AttributeTargets.Constructor, AllowMultiple = true)]
    public class RunAfterAttribute : Attribute
	{
		public readonly Type Requirement;
		public readonly int AtLeast;
		public RunAfterAttribute(Type requirement, int least = 1)
		{
            if (least < 1)
				throw new ArgumentException("At least less than one? doesn't make sense");
			Requirement = requirement;
			AtLeast = least;
		}
    }

    [AttributeUsage(AttributeTargets.Constructor, AllowMultiple = true)]
    public class RunAfterInstanceAttribute : Attribute
    {
        public readonly Type Requirement;
        public readonly Func<int, int> Instance;
        public readonly Type CountMeAs;
        public RunAfterInstanceAttribute(Type requirement, string instance, Type countMeAs = null)
        {
            Requirement = requirement;

            if (ScriptCache.scripts.ContainsKey(instance))
            {
                Instance = ScriptCache.scripts[instance];
            }
            else
            {
                var funcTask = CSharpScript.EvaluateAsync<Func<int, int>>("new System.Func<int, int>(" + instance + ")");
                funcTask.Wait();
                Instance = funcTask.Result;
                ScriptCache.scripts.Add(instance, Instance);
            }
            CountMeAs = countMeAs;
            
        }
    }

    [AttributeUsage(AttributeTargets.Constructor, AllowMultiple = true)]
    public class DontRunAfterAttribute : Attribute
	{
		public readonly Type Requirement;
		public readonly int AtMost;
		public DontRunAfterAttribute(Type requirement, int most = 0)
		{
         	if (most < 0)
				throw new ArgumentException("Negative most doesn't make sense");
			Requirement = requirement;
			AtMost = most;
		}
    }

    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = true)]
    public class TestIntegerRangeAttribute : Attribute
    {
        public int Min, Max;
    }

    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = true)]
    public class TestListRangeAttribute : Attribute
    {
        public object[] ValidArguments;
    }

    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
    public class TestIdAttribute : Attribute
    {
        public Type CountAs = null;
    }
}
