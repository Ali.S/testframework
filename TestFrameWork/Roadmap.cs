﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace TestFramework
{
    public class RoadMap
    {
        public TestItem[] Tests { get; }
        public RoadMap(TestItem[] tests)
        {
            for (var i = 0; i < tests.Length; i++)
            {
                var method = tests[i].TestType.GetMethod("runTest");
                var parameters = method.GetParameters();
                if (method == null || parameters.Length < 1 || parameters[0].ParameterType != typeof(Session))
                    throw new ArgumentException(tests[i].TestType.DeclaringType.ToString() + " Deos not define runTest method or it doesn't have Session Argument");
            }
            Tests = tests;
        }

        public Session createSessionWithRandomParameters()
        {
            var rand = new Random();
            Dictionary<TestItem, TestItem> userToFinalizedMap = new Dictionary<TestItem, TestItem>();
            TestItem[] completedTests = new TestItem[Tests.Length];
            for (var i = 0; i < Tests.Length; i++)
            {
                if (Tests[i].ConstructorArguements != null)
                    completedTests[i] = new TestItem(Tests[i].TestType, 
                        Tests[i].ConstructorArguements.Select(t => userToFinalizedMap[t]).ToArray(), 
                        Tests[i].TestArguements, 
                        Tests[i].ForcedDependencies?.Select(t => userToFinalizedMap[t]).ToArray());
                else
                {
                    completedTests[i] = new TestItem(Tests[i].SelectedConstructor, Tests[i].TestArguements, Tests[i].ForcedDependencies?.Select(t => userToFinalizedMap[t]).ToArray());
                }
                userToFinalizedMap.Add(Tests[i], completedTests[i]);
                if (Tests[i].TestArguements == null)
                {
                    var method = Tests[i].TestType.GetMethod("runTest");
                    var parameters = method.GetParameters();
                    var parameterValues = new object[parameters.Length - 1];
                    for (var j = 1; j < parameters.Length; j++)
                    {
                        var testId = parameters[j].GetCustomAttribute<TestIdAttribute>();
                        if (testId != null)
                        {
                            int id = 0;
                            var CountAs = testId.CountAs ?? Tests[i].TestType;
                            for (var k = 0; k < Tests.Length && k < i; k++)
                                if (Tests[k].TestType.IsAssignableFrom(CountAs))
                                    id++;
                            parameterValues[j - 1] = id;
                            continue;
                        }
                        var attributes = parameters[j].GetCustomAttributes().Where((a) =>
                            a is TestIntegerRangeAttribute ||
                            a is TestListRangeAttribute).ToArray();
                        parameterValues[j - 1] = null;
                        if (attributes.Length > 0)
                        {
                            var r = rand.Next() % attributes.Length;
                            var integerRange = attributes[r] as TestIntegerRangeAttribute;
                            if (integerRange != null)
                                parameterValues[j - 1] = rand.Next() % (integerRange.Max - integerRange.Min + 1) + integerRange.Min;
                            var listRange = attributes[r] as TestListRangeAttribute;
                            if (listRange != null)
                                parameterValues[j - 1] = listRange.ValidArguments[rand.Next() % listRange.ValidArguments.Length];
                        }
                    }
                    completedTests[i].TestArguements = parameterValues;
                }

            }
            return new Session(completedTests);
        }

        public List<Session> createAllSessions()
        {
            throw new NotImplementedException();
        }

    }
}
