﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace TestFramework
{
    public class TestItem
    {
        public Type TestType { get; private set; }
        public TestItem[] ConstructorArguements { get; private set; }
        public TestItem[] ForcedDependencies { get; private set; }
        internal ConstructorInfo SelectedConstructor { get; private set; }
        public object[] TestArguements { get; set; }

        public TestItem(Type test, TestItem[] constructorArguements = null, object[] testArguements = null, TestItem[] forcedDependencies = null)
        {
            TestType = test;
            if (constructorArguements != null)
            {
                SelectedConstructor = test.GetConstructor(constructorArguements.Select(i => i.TestType).ToArray());
                ConstructorArguements = constructorArguements;
                if (SelectedConstructor == null)
                    throw new ArgumentException("Selected constructor does not exist");
            }
            if (testArguements != null)
            {
                TestArguements = testArguements;
            }
            ForcedDependencies = forcedDependencies;
        }

        internal TestItem(ConstructorInfo constructor, object[] testArguements, TestItem[] forcedDependencies)
        {
            TestType = constructor.DeclaringType;
            SelectedConstructor = constructor;
            TestArguements = testArguements;
            ForcedDependencies = forcedDependencies;
        }
    }
}
