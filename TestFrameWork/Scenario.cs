﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Priority_Queue;

namespace TestFramework
{

    public class Scenario
	{
        class TreeNode
		{
			public ConstructorInfo constructor = null;
            public Type DeclaringType { get { return constructor?.DeclaringType; } }
            public IEnumerable<Type> TestTree
            {
                get
                {
                    var n = this;
                    while (n.parent != null)
                    {
                        yield return n.DeclaringType;
                        n = n.parent;
                    }
                }
            }

			public TreeNode parent = null;
			public List<TreeNode> branches = new List<TreeNode>();
            public Dictionary<Type, int> requiredInstanceCount = new Dictionary<Type, int>();
            public HashSet<Type> immediateRequirements = new HashSet<Type>();
            private static int IDCounter = 0;
            public readonly int ID = IDCounter++;
		}
		TestItem[] Tests;

        public Scenario(Type[] tests)
        {
            Tests = tests.Select(t => new TestItem(t)).ToArray();
        }

        public Scenario(TestItem[] tests)
        {
            Tests = tests;
        }

        class PriorityQueueNode : FastPriorityQueueNode
        {
            public TreeNode node;
            public int progress;
            public int cost = 0;
        }

        FastPriorityQueue<PriorityQueueNode> ActiveNodes = null;
        List<Dictionary<Type, int>> CheckedStartingInstanceNumbers = null;

        public RoadMap CreateRoadMap()
		{
            CheckedStartingInstanceNumbers = new List<Dictionary<Type, int>>();
            ActiveNodes = new FastPriorityQueue<PriorityQueueNode>(4096);
            Dictionary<Type, int> minRequiredInstances = new Dictionary<Type, int>();
            foreach (TestItem test in Tests)
            {
                var heirarchy = test.TestType;
                while (heirarchy != null)
                {
                    if (minRequiredInstances.ContainsKey(heirarchy))
                        minRequiredInstances[heirarchy]++;
                    else
                        minRequiredInstances.Add(heirarchy, 1);
                    heirarchy = heirarchy.BaseType;
                }
            }

            PushNewTreeToQueue(minRequiredInstances);

            TreeNode validPath = null;

            while (ActiveNodes.Count > 0)
            {
                var head = ActiveNodes.Dequeue();

                /*Console.WriteLine(
                    "{0} ({4}): {1} <- {2}: {3}",
                    head.cost,
                    head.node.parent?.ID,
                    head.node.ID,
                    string.Join(" ", head.node.TestTree.Select(s =>
                    {
                        if (s.Name.Length < 7)
                            return s.Name + string.Join(" ", new object[8 - s.Name.Length]);
                        else
                            return s.Name.Substring(0, 7);
                    })),
                    head.Priority);
                Console.WriteLine(
                    string.Join(" ", head.node.requiredInstanceCount.OrderByDescending(kvp => kvp.Value).Select(kvp =>
                    {
                        string k;
                        if (kvp.Key.Name.Length < 7)
                            k = kvp.Key.Name + string.Join(" ", new object[8 - kvp.Key.Name.Length]);
                        else
                            k = kvp.Key.Name.Substring(0, 7);
                        return string.Format("{0} {1:00}", k, kvp.Value);
                    }))
                );*/
                populateNodeBranches(head.node, head.progress != -1 ? Tests[head.progress] : null);
                Type me = head.node.constructor?.DeclaringType;
                for (var i = 0; i < head.node.branches.Count; i++)
                {
                    var expectedCost = head.cost + 1;
                    expectedCost += head.node.branches[i].requiredInstanceCount[typeof(object)];
                    var newProgress = head.progress;
                    if (head.progress >= 0 && (head.node.branches[i].constructor == Tests[head.progress].SelectedConstructor ||
                            (Tests[head.progress].SelectedConstructor == null && head.node.branches[i].constructor.DeclaringType == Tests[head.progress].TestType)
                            ))
                            newProgress--;
                    if (newProgress == -1 && head.node.branches[i].requiredInstanceCount[typeof(object)] == 0)
                    {
                        Console.WriteLine("Roadmap created, {0} branches were tested", head.node.branches[i].ID);
                        validPath = head.node.branches[i];
                        ActiveNodes.Clear();
                        break;
                    }
                    else
                    ActiveNodes.Enqueue(new PriorityQueueNode
                    {
                        node = head.node.branches[i],
                        progress = newProgress,
                        cost = head.cost + 1
                    }, expectedCost);
                }
			}

            Dictionary<TestItem, TestItem> userTestToFinalizedTests = new Dictionary<TestItem, TestItem>();
            var roadmap = new List<TestItem>();
            int progress = 0;
            while (validPath?.constructor != null)
            {
                TestItem item = null;
                bool generatedTest = true;
                if (progress < Tests.Length)
                {
                    if (validPath.constructor == Tests[progress].SelectedConstructor)
                    {
                        item = new TestItem(
                            validPath.constructor.DeclaringType, 
                            Tests[progress].ConstructorArguements.Select(a => userTestToFinalizedTests[a]).ToArray(), 
                            Tests[progress].TestArguements,
                            Tests[progress].ForcedDependencies?.Select(a => userTestToFinalizedTests[a]).ToArray());
                        generatedTest = false;
                    }
                    else
                    if (Tests[progress].SelectedConstructor == null && validPath.constructor.DeclaringType == Tests[progress].TestType)
                    {
                        item = new TestItem(validPath.constructor, Tests[progress].TestArguements,
                            Tests[progress].ForcedDependencies?.Select(a => userTestToFinalizedTests[a]).ToArray());
                        generatedTest = false;
                    }
                }

                if (generatedTest)
                {
                    item = new TestItem(validPath.constructor, null, null);
                }
                else
                {
                    userTestToFinalizedTests.Add(Tests[progress], item);
                    progress++;
                }

                roadmap.Add(item);
                validPath = validPath.parent;
            }

            ActiveNodes = null;
            CheckedStartingInstanceNumbers = null;

            if (roadmap.Count == 0)
                return null;
            else
                return new RoadMap(roadmap.ToArray());
		}

        private void populateNodeBranches(TreeNode node, TestItem UserTest)
        {
            Dictionary<Type, int> checkedRequirements = new Dictionary<Type, int>();
            if (UserTest != null)
            {
                var hierarchy = UserTest.TestType;
                bool invalidPath = false;
                while (hierarchy != null)
                {
                    checkedRequirements.Add(hierarchy, node.requiredInstanceCount[hierarchy]);
                    if (node.requiredInstanceCount[hierarchy] == 0)
                        invalidPath = true;
                    hierarchy = hierarchy.BaseType;
                }
                if (invalidPath)
                {
                    var newInstanceIds = new Dictionary<Type, int>(node.requiredInstanceCount);
                    hierarchy = UserTest.TestType;
                    while (hierarchy != null)
                    {
                        newInstanceIds[hierarchy]++;
                        hierarchy = hierarchy.BaseType;
                    }
                    PushNewTreeToQueue(newInstanceIds);
                    return;
                }
                var branches = CreateBranches(UserTest, node);
                if (branches != null)
                    node.branches.AddRange(branches.AsEnumerable());
                
            }

            var lookupOrder = node.requiredInstanceCount.OrderByDescending(pair =>
            {
                var depth = 0;
                var type = pair.Key;
                while (type != null)
                {
                    type = type.BaseType;
                    depth++;
                }
                return depth;
            });


            foreach (var t in lookupOrder)
            {
                if (t.Value > 0 && (!checkedRequirements.ContainsKey(t.Key) || checkedRequirements[t.Key] < t.Value))
                {
                    if (t.Key == typeof(object))
                        throw new Exception("badbadbad");
                    if (node.immediateRequirements.Contains(t.Key))
                    {
                        var branches = CreateBranches(new TestItem(t.Key), node);
                        if (branches != null)
                            node.branches.AddRange(branches.AsEnumerable());
                    }

                    var hierarchy = t.Key;
                    while (hierarchy != null)
                    {
                        if (checkedRequirements.ContainsKey(hierarchy))
                            checkedRequirements[hierarchy] += t.Value;
                        else
                            checkedRequirements.Add(hierarchy, node.requiredInstanceCount[hierarchy]);
                        hierarchy = hierarchy.BaseType;
                    }
                }
            }
        }

        private bool checkIfInstanceRequirementsAreFeasable(Dictionary<Type, int> expectedInstanceIds, Dictionary<Type, int> instanceRequirements, Dictionary<Type, int> expectedInstanceIdsAtRoot)
        {
            Dictionary<Type, int> missingInstances = new Dictionary<Type, int>();
            var lookupOrder = instanceRequirements.OrderByDescending(pair =>
            {
                var depth = 0;
                var type = pair.Key;
                while (type != null)
                {
                    type = type.BaseType;
                    depth++;
                }
                return depth;
            });

            foreach (var requirement in lookupOrder)
            {
                int missing = 0;
                var req = instanceRequirements.ContainsKey(requirement.Key) ? instanceRequirements[requirement.Key] + 1 : 0;
                var availble = expectedInstanceIds.ContainsKey(requirement.Key) ? expectedInstanceIds[requirement.Key] : 0;
                availble += missingInstances.ContainsKey(requirement.Key) ? missingInstances[requirement.Key] : 0;
                missing = req - availble;
                if (missing > 0)
                {
                    Type heirarchy = requirement.Key;
                    while (heirarchy != null)
                    {
                        if (missingInstances.ContainsKey(heirarchy))
                            missingInstances[heirarchy] += missing;
                        else
                            missingInstances.Add(heirarchy, missing);
                        heirarchy = heirarchy.BaseType;
                    }
                }
            }

            if (missingInstances.Count > 0)
            {
                var newInstanceIds = new Dictionary<Type, int>(expectedInstanceIdsAtRoot);
                foreach (var missing in missingInstances)
                {
                    if (newInstanceIds.ContainsKey(missing.Key))
                        newInstanceIds[missing.Key] += missing.Value;
                    else
                        newInstanceIds.Add(missing.Key, missing.Value);
                }
                PushNewTreeToQueue(newInstanceIds);
                return false;
            }

            return true;
        }

        private List<TreeNode> CreateBranches(TestItem test, TreeNode parent)
		{
            var requiredInstanceIds = new Dictionary<Type, int>(parent.requiredInstanceCount);
            var root = parent;
            while (root.parent != null)
                root = root.parent;

            {
                Type heirarchy = test.TestType;
                while (heirarchy != null)
                {
                    requiredInstanceIds[heirarchy]--;
                    heirarchy = heirarchy.BaseType;
                }
            }

            var rootInstanceIds = root.requiredInstanceCount;
                
            
            List<TreeNode> res = new List<TreeNode>();
            ConstructorInfo[] constructors;

            if (test.SelectedConstructor == null)
                constructors = test.TestType.GetConstructors();
            else
                constructors = new ConstructorInfo[] { test.SelectedConstructor };

            for (int i = 0; i < constructors.Length; i++)
			{
                var constrcutorCanBeUsed = true;
                TreeNode option = new TreeNode();
				Dictionary<Type, int> constructorRequirements = new Dictionary<Type, int>();

                foreach (var attrib in constructors[i].GetCustomAttributes())
                {
                    if (attrib is RunAfterAttribute runAfter)
                    {
                        if (runAfter.AtLeast < 0)
                            throw new InvalidOperationException("At least can not be less thatn zero");
                        if (constructorRequirements.ContainsKey(runAfter.Requirement))
                            constructorRequirements[runAfter.Requirement] = Math.Max(runAfter.AtLeast - 1, constructorRequirements[runAfter.Requirement]);
                        else
                            constructorRequirements.Add(runAfter.Requirement, runAfter.AtLeast - 1);
                    }

                    if (attrib is DontRunAfterAttribute dontRunAfter)
                    {
                        var remainingRequirementRuns = requiredInstanceIds.ContainsKey(dontRunAfter.Requirement) ? requiredInstanceIds[dontRunAfter.Requirement] : 0;

                        if (remainingRequirementRuns > dontRunAfter.AtMost)
                        {
                            constrcutorCanBeUsed = false;
                            break;
                        }

                    }

                    if (attrib is RunAfterInstanceAttribute runAfterInstance)
                    {
                        Type counter = runAfterInstance.CountMeAs ?? test.TestType;
                        if (!counter.IsAssignableFrom(test.TestType))
                            throw new InvalidOperationException(runAfterInstance.CountMeAs.ToString() + " is not assignable from " + test.ToString());
                        int instance = runAfterInstance.Instance(requiredInstanceIds[counter]);
                        if (instance < 0)
                        {
                            constrcutorCanBeUsed = false;
                            break;
                        }

                        if (constructorRequirements.ContainsKey(runAfterInstance.Requirement))
                            constructorRequirements[runAfterInstance.Requirement] = Math.Max(instance, constructorRequirements[runAfterInstance.Requirement]);
                        else
                            constructorRequirements.Add(runAfterInstance.Requirement, instance);
                    }
                }

                if (!constrcutorCanBeUsed)
                    continue;
                if (test.SelectedConstructor == null)
                {
                    foreach (var parameter in constructors[i].GetParameters())
                    {
                        bool specificInstanceRequirement = false;
                        foreach (var attrib in parameter.GetCustomAttributes())
                        {
                            if (attrib is InstanceRequirementAttribute instanceRequirement)
                            {
                                Type counter = instanceRequirement.CountMeAs ?? test.TestType;
                                if (!counter.IsAssignableFrom(test.TestType))
                                    throw new InvalidOperationException(instanceRequirement.CountMeAs.ToString() + " is not assignable from " + test.ToString());
                                int instance = instanceRequirement.Instance(requiredInstanceIds[counter]);
                                if (instance < 0)
                                {
                                    constrcutorCanBeUsed = true;
                                    break;
                                }

                                if (constructorRequirements.ContainsKey(parameter.ParameterType))
                                    constructorRequirements[parameter.ParameterType] = Math.Max(instance, constructorRequirements[parameter.ParameterType]);
                                else
                                    constructorRequirements.Add(parameter.ParameterType, instance);

                                specificInstanceRequirement = false;
                            }
                        }

                        if (!specificInstanceRequirement)
                        {
                            if (!constructorRequirements.ContainsKey(parameter.ParameterType))
                                constructorRequirements.Add(parameter.ParameterType, 0);
                        }
                        if (!constrcutorCanBeUsed)
                            break;
                    }
                }
                else
                {
                    foreach (var parameter in constructors[i].GetParameters())
                        if (!constructorRequirements.ContainsKey(parameter.ParameterType))
                            constructorRequirements.Add(parameter.ParameterType, 0);
                }

                if (!constrcutorCanBeUsed)
                    continue;
                if (!checkIfInstanceRequirementsAreFeasable(requiredInstanceIds, constructorRequirements, rootInstanceIds))
                    continue;
                
                var newNode = new TreeNode();

                var immediateRequirements = new HashSet<Type>(parent.immediateRequirements);
                immediateRequirements.RemoveWhere(t => t.IsAssignableFrom(test.TestType));
                immediateRequirements.UnionWith(constructorRequirements.Select(r => r.Key));
				
				newNode.constructor = constructors[i];
				newNode.parent = parent;
                newNode.requiredInstanceCount = requiredInstanceIds;
                newNode.immediateRequirements = immediateRequirements;

                res.Add(newNode);
			}

            return res;
		}

		private void merge<T>(List<T> a, List<T> b) where T : IComparable<T>
		{
			if (a == null || b == null)
				return;
			var originalCount = a.Count;
			var it1 = 0;
			var it2 = 0;
			while (it2 < b.Count && it1 < originalCount)
			{
				if (a[it1].CompareTo(b[it2]) < 0)
					it1++;
				else if (a[it1].CompareTo(b[it2]) > 0)
				{
					a.Add(b[it2]);
					it2++;
				}
				else
				{
					it1++;
					it2++;
				}
			}
			while (it2 < b.Count)
				a.Add(b[it2++]);
			a.Sort();
        }

        private void PushNewTreeToQueue(Dictionary<Type, int> newIntanceIds)
        {
            for (var i = 0; i < CheckedStartingInstanceNumbers.Count; i++)
            {
                if (DictionaryEquals(newIntanceIds, CheckedStartingInstanceNumbers[i]))
                    return;
            }
            CheckedStartingInstanceNumbers.Add(newIntanceIds);
            TreeNode root = new TreeNode();
            root.requiredInstanceCount = newIntanceIds;

            ActiveNodes.Enqueue(new PriorityQueueNode
            {
                node = root,
                progress = Tests.Length - 1,
                cost = 0
            }, newIntanceIds[typeof(object)]);
        }
        private static bool DictionaryEquals(Dictionary<Type, int> a, Dictionary<Type, int> b)
        {
            if (a.Count != b.Count)
                return false;
            foreach (var kvp in a)
            {
                if (!b.ContainsKey(kvp.Key) || b[kvp.Key] != kvp.Value)
                    return false;
            }
            return true;
        }
    }
}
