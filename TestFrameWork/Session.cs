﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace TestFramework
{
    public class Session : IDisposable
	{
		struct TestInstance
		{
            public object Instance;
            public double Duration { get
                {
                    return (End - Start).TotalSeconds;
                }
            }
            public DateTime Start;
            public DateTime End;
        }

		Dictionary<Type, List<Task<TestInstance>>> tests = null;
        Dictionary<TestItem, Task<TestInstance>> execution = null;

        public T GetTestInstance<T>(int id) where T : class 
        {
            if (tests.ContainsKey(typeof(T)) && id < tests[typeof(T)].Count)
            {
                var task = tests[typeof(T)][id];
                if (task.IsCompleted)
                    return (T)task.Result.Instance;
                else
                    return null;
            }
            else
                return null;
        }

        private TestItem[] Roadmap;

		public Session(TestItem[] roadmap)
		{
            Roadmap = roadmap;
		}
        
        public Task Run()
        {
            tests = new Dictionary<Type, List<Task<TestInstance>>>();
            execution = new Dictionary<TestItem, Task<TestInstance>>();
            for (var i = 0; i < Roadmap.Length; i++)
            {
                var constructor = Roadmap[i].SelectedConstructor;
                var constructorParameterValues = new Task<TestInstance>[Roadmap[i].SelectedConstructor.GetParameters().Length];

                if (Roadmap[i].ConstructorArguements == null)
                {
                    var constructorParameters = constructor.GetParameters();
                    for (var j = 0; j < constructorParameters.Length; j++)
                    {
                        constructorParameterValues[j] = null;
                        foreach (Attribute a in constructorParameters[j].GetCustomAttributes())
                        {
                            var instanceRequirement = a as InstanceRequirementAttribute;
                            if (instanceRequirement != null)
                            {
                                Type counter = instanceRequirement.CountMeAs ?? constructor.DeclaringType;
                                if (!counter.IsAssignableFrom(constructor.DeclaringType))
                                    throw new InvalidOperationException(instanceRequirement.CountMeAs.ToString() + " is not assignable from " + constructor.DeclaringType.ToString());
                                int instanceId = instanceRequirement.Instance(tests[counter].Count);
                                if (!tests.ContainsKey(constructorParameters[j].ParameterType) || instanceId < 0 || instanceId >= tests[constructorParameters[j].ParameterType].Count)
                                    throw new InvalidOperationException("instanceRequirement resulted in non-existing instance");
                                constructorParameterValues[j] = tests[constructorParameters[j].ParameterType][instanceId];
                                break;
                            }
                        }
                        if (constructorParameterValues[j] == null)
                        {
                            constructorParameterValues[j] = tests[constructorParameters[j].ParameterType].Last();
                        }
                    }
                }
                else
                {
                    for (var j = 0; j < Roadmap[i].ConstructorArguements.Length; j++)
                        constructorParameterValues[j] = execution[Roadmap[i].ConstructorArguements[j]];
                }

                
                IEnumerable<Task<TestInstance>> requirements = null;

                if (constructorParameterValues.Length > 0)
                    requirements = constructorParameterValues.AsEnumerable();
                if (Roadmap[i].ForcedDependencies != null && Roadmap[i].ForcedDependencies.Length > 0)
                    if (requirements != null)
                        requirements = requirements.Concat(Roadmap[i].ForcedDependencies?.Select(f => execution[f]));
                    else
                        requirements = Roadmap[i].ForcedDependencies?.Select(f => execution[f]);

                Task<TestInstance[]> requirementsTask;
                if (requirements != null)
                    requirementsTask = Task.WhenAll(requirements);
                else
                    requirementsTask = Task.Run(() => new TestInstance[0]);

                var testMethod = Roadmap[i].TestType.GetMethod("runTest");

                var parameters = testMethod.GetParameters();
                var parameterValues = new object[parameters.Length];
                parameterValues[0] = this;
                for (var j = 1; j < parameters.Length; j++)
                {
                    parameterValues[j] = Roadmap[i].TestArguements[j - 1];
                }

                Task<TestInstance> runTask = requirementsTask.ContinueWith(values =>{
                    var instance = constructor.Invoke(constructorParameterValues.Select(t => t.Result.Instance).ToArray());
                    DateTime start = DateTime.Now;
                    testMethod.Invoke(instance, parameterValues);
                    DateTime end = DateTime.Now;
                    var testInstance = new TestInstance { Instance =  instance, Start = start, End = end};

                    return testInstance;
                });
                
                {
                    var heirarchy = constructor.DeclaringType;
                    while (heirarchy != null)
                    {
                        if (tests.ContainsKey(heirarchy))
                            tests[heirarchy].Add(runTask);
                        else
                            tests.Add(heirarchy, new List<Task<TestInstance>> { runTask });
                        heirarchy = heirarchy.BaseType;
                    }
                }
                execution.Add(Roadmap[i], runTask);
            }
            return execution[Roadmap[Roadmap.Length - 1]];
        }

        public Dictionary<Type, double> getRunTimes()
        {
            return Roadmap
                .Select(item => item.TestType)
                .Distinct()
                .ToDictionary(type => type, type => tests[type].Average(instance => instance.Result.Duration));
        }

        public void Dispose()
        {
            if (tests != null)
            {
                foreach (var t in tests)
                {
                    if (typeof(IDisposable).IsAssignableFrom(t.Key))
                    {
                        foreach (var e in t.Value)
                        {
                            if (e.IsCompleted)
                                ((IDisposable)e.Result.Instance).Dispose();
                            else
                                throw new InvalidOperationException("Can not dispose while tasks are running");
                        }
                    }
                }
            }
        }
    }
}
