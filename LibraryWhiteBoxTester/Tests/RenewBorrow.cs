﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;
using LibraryApp;

namespace LibraryWhiteboxTester.Tests
{
    class RenewBorrow
    {
        public User User { get; private set; }
        public Title Title{ get; private set; }
        public RenewBorrow(BorrowTitle PreviousBorrow)
        {
            User = PreviousBorrow.User;
            Title = PreviousBorrow.Title;
        }

        public void runTest(Session session)
        {
            User.RenewBorrow(Title);
        }
    }
}
