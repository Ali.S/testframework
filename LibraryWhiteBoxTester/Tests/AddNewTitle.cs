﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;
using LibraryApp;

namespace LibraryWhiteboxTester.Tests
{
    class AddNewTitle
    {
        public Library Library { get; private set; }
        public Title Title { get; private set; }
        public AddNewTitle(CreateLibrary createLibrary)
        {
            Library = createLibrary.Library;
        }

        public void runTest(Session session, [TestId] int id)
        {
            var isbn = "t_" + id.ToString();
            Library.addTitle(isbn);
            Title = Library.getTitle(isbn);
            if (Title.ISBN != isbn)
                throw new Exception("incorrect title was created");
        }
    }
}
