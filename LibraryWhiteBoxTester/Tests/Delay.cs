﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;

namespace LibraryWhiteboxTester.Tests
{
    class Delay
    {
        TimeSpan Duration;
        public void runTest(Session session, TimeSpan duration)
        {
            System.Threading.Thread.Sleep(duration);
            Duration = duration;
        }
    }
}
