﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;
using LibraryApp;

namespace LibraryWhiteboxTester.Tests
{
    class AddExistingTitle
    {
        public Library Library { get; private set; }
        public Title ExistingTitle { get; private set; }
        public AddExistingTitle(AddNewTitle existingTitle)
        {
            Library = existingTitle.Library;
            ExistingTitle = existingTitle.Title;
        }

        public void runTest(Session session)
        {
            try
            {
                Library.addTitle(ExistingTitle.ISBN);
            }
            catch (Exception)
            {
                return;
            }
            throw new Exception("Duplicate title exception was not thrown");

        }
    }
}
