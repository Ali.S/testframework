﻿using System;
using System.Collections.Generic;
using System.Text;
using LibraryApp;
using TestFramework;

namespace LibraryWhiteboxTester.Tests
{
    class AddNewUser
    {
        public Library Library { get; private set; }
        public User User { get; private set; }
        public AddNewUser(CreateLibrary library)
        {
            Library = library.Library;
        }

        public void runTest(Session session, [TestFramework.TestId] int testId)
        {
            string userId = "u_" + testId.ToString();
            Library.addUser(userId);
            User = Library.getUser(userId);
            if (User.UserId != userId)
                throw new Exception("Incorrect User was created");
        }
    }
}
