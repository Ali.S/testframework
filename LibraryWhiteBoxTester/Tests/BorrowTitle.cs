﻿using System;
using System.Collections.Generic;
using System.Text;
using LibraryApp;
using TestFramework;
using System.Linq;

namespace LibraryWhiteboxTester.Tests
{
    class BorrowTitle
    {
        public User User { get; private set; }
        public Title Title { get; private set; }
        public BorrowTitle(AddNewUser userCreation, AddNewTitle titleCreation)
        {
            User = userCreation.User;
            Title = titleCreation.Title;
        }
        public void runTest(Session session)
        {
            var previousBorrows = User.BorrowedTitles.ToArray();
            try
            {
                User.Borrow(Title);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Print(e.ToString());
                return;
            }
            var newBorrows = User.BorrowedTitles.Except(previousBorrows).ToList();
            if (newBorrows.Count != 1)
                throw new Exception("Borrow failed without error");
            if (newBorrows[0] != Title)
                throw new Exception("Invalid title was borrowed");
        }
    }
}
