﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;
using LibraryApp;

namespace LibraryWhiteboxTester.Tests
{
    class AddExistingUser
    {
        public Library Library { get; private set; }
        public User ExistingUser { get; private set; }
        public AddExistingUser(AddNewUser existingUser)
        {
            Library = existingUser.Library;
            ExistingUser = existingUser.User;
        }

        public void runTest(Session session)
        {
            try
            {
                Library.addUser(ExistingUser.UserId);
            }
            catch (Exception)
            {
                return;
            }
            throw new Exception("Duplicate user exception was not thrown");
        }
    }
}
