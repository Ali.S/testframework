﻿using LibraryApp;
using TestFramework;
using System.Linq;

namespace LibraryWhiteboxTester.Tests
{
    class CheckFine
    {
        User User;
        public CheckFine(BorrowTitle borrow)
        {
            User = borrow.User;
        }

        public void runTest(Session session, int expectedFine)
        {
            var fine = User.TotalFine;
            if (fine != expectedFine)
                throw new System.Exception(string.Format("Expected {0} but fine is {1}", expectedFine, fine));
        }
    }
}