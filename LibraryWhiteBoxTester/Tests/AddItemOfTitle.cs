﻿using System;
using LibraryApp;
using TestFramework;

namespace LibraryWhiteboxTester.Tests
{
    internal class AddItemOfTitle
    {
        public Title Title { get; private set; }
        public AddItemOfTitle(AddNewTitle title)
        {
            Title = title.Title;
        }

        public void runTest(Session session, [TestIntegerRange(Min = 1, Max = 1)] int numCopies)
        {
            int oldCopyCount = Title.ItemsAvailable;
            Title.AddCopyToInventory(numCopies);
            if (numCopies + oldCopyCount != Title.ItemsAvailable)
                throw new Exception("incorrect number of copies were created");
        }
    }
}