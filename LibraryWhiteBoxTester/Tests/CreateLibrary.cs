﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;

namespace LibraryWhiteboxTester.Tests
{
    class CreateLibrary
    {
        public LibraryApp.Library Library { get; private set; }
        public CreateLibrary()
        {
        }

        public void runTest(Session session)
        {
            Library = new LibraryApp.Library();
        }
    }
}
