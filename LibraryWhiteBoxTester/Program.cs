﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;
using LibraryWhiteboxTester.Tests;

namespace LibraryWhiteboxTester
{
    class Program
    {
        static void Main(string[] args)
        {
            LibraryApp.TestTimeUtil.TimeSkipMultiplier = 3600 * 24 * 14; // 14 day is 1 second
            List<TestItem[]> defs = new List<TestItem[]>();

            defs.Add(new TestItem[] { new TestItem(typeof(AddNewUser)) });
            defs.Add(new TestItem[] { new TestItem(typeof(AddExistingUser)) });
            defs.Add(new TestItem[] { new TestItem(typeof(AddNewTitle)) });
            defs.Add(new TestItem[] { new TestItem(typeof(AddNewUser)) });
            
            {
                TestItem createUserA = new TestItem(typeof (AddNewUser));
                TestItem createUserB = new TestItem(typeof (AddNewUser));
                TestItem createTitle = new TestItem(typeof (AddNewTitle));
                TestItem createCopies = new TestItem(typeof (AddItemOfTitle), new TestItem[] { createTitle }, new object[] { 2 });
                TestItem Borrow1 = new TestItem(typeof(BorrowTitle), new TestItem[] { createUserA, createTitle }, null, new TestItem[] { createCopies });
                TestItem Borrow2 = new TestItem(typeof(BorrowTitle), new TestItem[] { createUserB, createTitle }, null, new TestItem[] { createCopies });
                defs.Add(new TestItem[] { createUserA, createUserB, createTitle, createCopies, Borrow1, Borrow2 });
            }
            {
                TestItem createItems = new TestItem(typeof(AddItemOfTitle), null, new object[] { 1 });
                TestItem borrow = new TestItem(typeof(BorrowTitle), null, null, new TestItem[] { createItems });
                TestItem delay1ExtraDay = new TestItem(
                    typeof(Delay), 
                    null, 
                    new object[] { TimeSpan.FromMilliseconds((LibraryApp.Borrow.AllowedLoanTime + TimeSpan.FromDays(1)).TotalMilliseconds / LibraryApp.TestTimeUtil.TimeSkipMultiplier) }, 
                    new TestItem[] { borrow });

                TestItem fine = new TestItem(typeof(CheckFine), null, new object[] { LibraryApp.Borrow.DelayCost }, new TestItem[] { delay1ExtraDay });
                defs.Add(new TestItem[] { createItems, borrow, delay1ExtraDay, fine });
            }

            for (var i = 0; i < defs.Count; i++)
            {
                Scenario scenario = new Scenario(defs[i]);
                var roadMap = scenario.CreateRoadMap();
                Console.WriteLine(string.Join(" => ", roadMap.Tests.Select(t => t.TestType)));

                Dictionary<Type, double> runTimes = new Dictionary<Type, double>();
                foreach (var type in roadMap.Tests.Select(c => c.TestType).Distinct())
                    runTimes.Add(type, 0);

                bool AllTestsPassed = true;
                int repeat = 10;
                for (int k = 0; k < repeat; k++)
                {
                    var session = roadMap.createSessionWithRandomParameters();
                    var run = session.Run();
                    run.Wait();
                    if (!run.IsFaulted)
                    {
                        foreach (var item in session.getRunTimes())
                            runTimes[item.Key] += item.Value;
                    }
                    else
                    {
                        AllTestsPassed = false;
                        Console.WriteLine(run.Exception.ToString());
                        break;
                    }
                }

                Console.WriteLine("Test results #{0}", i);

                if (AllTestsPassed)
                {

                    foreach (var item in runTimes)
                        Console.WriteLine("\t {0}: {1:0.00}", item.Key, item.Value / repeat);
                }
            }
        }
    }
}
