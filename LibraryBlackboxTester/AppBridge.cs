﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryBlackboxTester
{
    public class AppBridge : IDisposable
    {
        System.Diagnostics.Process clientProcess;
        public AppBridge(string application)
        {
            System.Diagnostics.ProcessStartInfo pi = new System.Diagnostics.ProcessStartInfo();
            pi.FileName = application;
            string fullPath = System.IO.Path.GetFullPath(application);
            pi.WorkingDirectory = fullPath.Substring(0, fullPath.LastIndexOf(System.IO.Path.DirectorySeparatorChar));
            pi.UseShellExecute = false;
            pi.RedirectStandardInput = true;
            pi.RedirectStandardOutput = true;
            pi.RedirectStandardError = true;
            pi.CreateNoWindow = true;
            pi.FileName = application;
            clientProcess = System.Diagnostics.Process.Start(pi);
        }

        public void Dispose()
        {
            if (Running)
                clientProcess.Kill();
            clientProcess.Dispose();
        }

        public System.IO.StreamWriter Input { get { return clientProcess.StandardInput; } }
        public System.IO.StreamReader Output { get { return clientProcess.StandardOutput; } }
        public bool Running { get { return !clientProcess.HasExited; } }

    }
}
