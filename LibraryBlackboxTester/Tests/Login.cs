﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    class Login : IDisposable
    {
        public AppBridge AppBridge { get; private set; }
        public string userId { get; private set; }
        
        public Login(CreateServer server)
        {
            userId = "root";
        }
        
        public Login(CreateServer server, AddNewUser user)
        {
            userId = user.UserID;
        }

        public void runTest(Session session)
        {
            AppBridge = new AppBridge(Program.LibraryAppPath);
            string output = AppBridge.Output.ReadLine(); // server or client
            AppBridge.Input.WriteLine("c");
            output = AppBridge.Output.ReadLine(); // ip
            AppBridge.Input.WriteLine("127.0.0.1");
            output = AppBridge.Output.ReadLine(); // ?
            AppBridge.Input.WriteLine("login " + userId);
            string welcome = AppBridge.Output.ReadLine();
            if (welcome != "welcome " + userId)
                throw new Exception("Invalid login message");
            output = AppBridge.Output.ReadLine(); // ?
        }

        public void Dispose()
        {
            AppBridge.Dispose();
        }
    }
}
