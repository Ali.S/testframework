﻿using System;
using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    internal class AddItemOfTitle
    {
        public AppBridge AppBridge { get; private set; }
        public string ISBN { get; private set; }
        public AddItemOfTitle(AddNewTitle title)
        {
            AppBridge = title.AppBridge;
            ISBN = title.ISBN;
        }

        public void runTest(Session session, [TestIntegerRange(Min = 1, Max = 1)] int numCopies)
        {
            AppBridge.Input.WriteLine("list titles");
            int existingItemCount = -1;
            string output;
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                {
                    string isbn = output.Split(' ')[0];
                    if (isbn == ISBN)
                    {
                        existingItemCount = int.Parse(output.Split(' ')[2]);
                    }
                }
            };
            if (existingItemCount < 0)
                throw new Exception("Invalid itemCount");
            AppBridge.Input.WriteLine("create items " + ISBN + " " + numCopies.ToString());
            output = AppBridge.Output.ReadLine(); // done
            if (output != "done")
                throw new Exception("unexpected result from server: " + output);
            output = AppBridge.Output.ReadLine(); // ?
            AppBridge.Input.WriteLine("list titles");
            int newItemCount = -1;
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                {
                    string isbn = output.Split(' ')[0];
                    if (isbn == ISBN)
                    {
                        newItemCount = int.Parse(output.Split(' ')[2]);
                    }
                }
            };
            if (newItemCount != existingItemCount + numCopies)
                throw new Exception("items were not added as expeceted");
        }
    }
}