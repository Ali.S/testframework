﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    class CreateServer : IDisposable
    {
        public AppBridge AppBridge { get; private set; }
        public CreateServer()
        {
        }

        public void runTest(Session session)
        {
            AppBridge = new AppBridge(Program.LibraryAppPath);
            AppBridge.Input.WriteLine("s");
            var login = new Login(this);
            login.runTest(null);
            AppBridge timeSetting = login.AppBridge;
            timeSetting.Input.WriteLine("set_time_scale " + Program.TimeSkipMultiplier);
            timeSetting.Dispose();
        }

        public void Dispose()
        {
            AppBridge.Dispose();
        }
    }
}
