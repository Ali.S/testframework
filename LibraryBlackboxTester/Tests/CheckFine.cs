﻿using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    class CheckFine
    {
        AppBridge AppBridge;
        
        public CheckFine(Login login)
        {
            AppBridge = login.AppBridge;
        }

        public void runTest(Session session, int expectedFine)
        {
            AppBridge.Input.WriteLine("list borrows");
            string output;
            int fine = 0;
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                {
                    if (output.StartsWith("Total fine = "))
                        fine = int.Parse(output.Split(' ')[3]);
                }
            };
            if (fine != expectedFine)
                throw new System.Exception(string.Format("Expected {0} but fine is {1}", expectedFine, fine));
        }
    }
}