﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    class AddExistingUser
    {
        public AppBridge AppBridge { get; private set; }
        public string UserID { get; private set; }
        public AddExistingUser(AddNewUser addNewUser)
        {
            AppBridge = addNewUser.AppBridge;
            UserID = addNewUser.UserID;
        }

        public void runTest(Session session)
        {
            AppBridge.Input.WriteLine("create user " + UserID);
            string output = AppBridge.Output.ReadLine(); // done
            if (!output.Contains("Error: Duplicate User"))
                throw new Exception("unexpected result from server: " + output);
            AppBridge.Output.ReadLine(); // ?
        }
    }
}
