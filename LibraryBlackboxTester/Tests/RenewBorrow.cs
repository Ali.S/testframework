﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    class RenewBorrow
    {
        public AppBridge AppBridge { get; private set; }
        public string ISBN { get; private set; }
        public RenewBorrow(Login login, AddNewTitle title)
        {
            AppBridge = login.AppBridge;
            ISBN = title.ISBN;
        }

        public void runTest(Session session)
        {
            AppBridge.Input.WriteLine("renew " + ISBN);
            string output = AppBridge.Output.ReadLine();
            if (output != "done")
                throw new Exception("Invalid server repsonse: " + output);
            AppBridge.Output.ReadLine(); // ?
        }
    }
}
