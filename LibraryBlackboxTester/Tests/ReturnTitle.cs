﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;
using System.Linq;

namespace LibraryBlackboxTester.Tests
{
    class ReturnTitle
    {
        public AppBridge AppBridge { get; private set; }
        public string ISBN { get; private set; }
        public ReturnTitle(Login login, AddNewTitle title)
        {
            AppBridge = login.AppBridge;
            ISBN = title.ISBN;
        }

        public void runTest(Session session)
        {
            AppBridge.Input.WriteLine("list borrows");
            List<string> existingBorrows = new List<string>();
            string output;
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                    existingBorrows.Add(output.Split(' ')[0]);
            };
            AppBridge.Input.WriteLine("borrow " + ISBN);
            output = AppBridge.Output.ReadLine(); // done
            if (output != "done")
                throw new Exception("unexpected result from server: " + output);
            output = AppBridge.Output.ReadLine(); // ?
            AppBridge.Input.WriteLine("list borrows");
            List<string> newBorrows = new List<string>();
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                    newBorrows.Add(output.Split(' ')[0]);
            };
            if (!existingBorrows.Except(newBorrows).Contains(ISBN))
                throw new Exception("title was not returned as expeceted");
        }
    }
}
