﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    class Delay
    {
        TimeSpan Duration;
        public void runTest(Session session, TimeSpan duration)
        {
            System.Threading.Thread.Sleep(duration);
            Duration = duration;
        }
    }
}
