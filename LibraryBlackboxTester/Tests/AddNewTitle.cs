﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;
using System.Linq;

namespace LibraryBlackboxTester.Tests
{
    class AddNewTitle
    {
        public AppBridge AppBridge { get; private set; }
        public string ISBN { get; private set; }
        public AddNewTitle(Login login)
        {
            AppBridge = login.AppBridge;
        }

        public void runTest(Session session, [TestId] int id)
        {
            var isbn = "t_" + id.ToString();
            AppBridge.Input.WriteLine("list titles");
            List<string> existingTitles = new List<string>();
            string output;
            while(true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                    existingTitles.Add(output.Split(' ')[0]);
            };
            AppBridge.Input.WriteLine("create title " + isbn);
            output = AppBridge.Output.ReadLine(); // done
            if (output != "done")
                throw new Exception("unexpected result from server: " + output);
            output = AppBridge.Output.ReadLine(); // ?
            AppBridge.Input.WriteLine("list titles");
            List<string> newTitles = new List<string>();
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                    newTitles.Add(output.Split(' ')[0]);
            };
            if (!newTitles.Except(existingTitles).Contains(isbn))
                throw new Exception("title was not added as expeceted");
            ISBN = isbn;
        }
    }
}
