﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;
using System.Linq;

namespace LibraryBlackboxTester.Tests
{
    class AddNewUser
    {
        public AppBridge AppBridge { get; private set; }
        public string UserID { get; private set; }
        public AddNewUser(Login login)
        {
            AppBridge = login.AppBridge;
        }

        public void runTest(Session session, [TestFramework.TestId] int testId)
        {
            var userId = "u_" + testId.ToString();
            AppBridge.Input.WriteLine("list users");
            List<string> existingUsers = new List<string>();
            string output;
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                
                if (output == "?")
                    break;
                else
                    existingUsers.Add(output.Split(' ')[0]);
            } ;
            AppBridge.Input.WriteLine("create user " + userId);
            output = AppBridge.Output.ReadLine(); // done
            if (output != "done")
                throw new Exception("unexpected result from server: " + output);
            output = AppBridge.Output.ReadLine(); // ?
            AppBridge.Input.WriteLine("list users");
            List<string> newTitles = new List<string>();
            while (true)
            {
                output = AppBridge.Output.ReadLine();
                if (output == "?")
                    break;
                else
                    newTitles.Add(output.Split(' ')[0]);
            };
            if (!newTitles.Except(existingUsers).Contains(userId))
                throw new Exception("title was not added as expeceted");
            UserID = userId;
        }
    }
}
