﻿using System;
using System.Collections.Generic;
using System.Text;
using TestFramework;

namespace LibraryBlackboxTester.Tests
{
    class AddExistingTitle
    {
        public AppBridge AppBridge { get; private set; }
        public string ISBN { get; private set; }
        public AddExistingTitle(AddNewTitle addNewTitle)
        {
            AppBridge = addNewTitle.AppBridge;
            ISBN = addNewTitle.ISBN;
        }

        public void runTest(Session session)
        {
            AppBridge.Input.WriteLine("create title " + ISBN);
            string output = AppBridge.Output.ReadLine(); // done
            if (!output.Contains("Error: Duplicate Title"))
                throw new Exception("unexpected result from server: " + output);
            AppBridge.Output.ReadLine() ; // ?
        }
    }
}
