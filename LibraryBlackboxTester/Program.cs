﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestFramework;
using LibraryBlackboxTester.Tests;

namespace LibraryBlackboxTester
{
    class Program
    {
        internal static readonly string LibraryAppPath = "../../../LibraryApp/bin/debug/net461/LibraryApp.exe";
        internal static readonly int TimeSkipMultiplier = 3600 * 24 * 14; // 14 day is 1 second

        static void Main(string[] args)
        {
            List<TestItem[]> defs = new List<TestItem[]>();

            defs.Add(new TestItem[] { new TestItem(typeof(AddNewUser)) });
            defs.Add(new TestItem[] { new TestItem(typeof(AddExistingUser)) });
            defs.Add(new TestItem[] { new TestItem(typeof(AddNewTitle)) });
            defs.Add(new TestItem[] { new TestItem(typeof(AddNewUser)) });

            {
                TestItem createServer = new TestItem(typeof(CreateServer));
                TestItem rootLogin = new TestItem(typeof(Login));
                TestItem createUserA = new TestItem(typeof(AddNewUser), new TestItem[] { rootLogin });
                TestItem createUserB = new TestItem(typeof(AddNewUser), new TestItem[] { rootLogin }, null, new TestItem[] { createUserA }); // users are handled by the same client and should not run in parallel
                TestItem loginA = new TestItem(typeof(Login), new TestItem[] { createServer, createUserA });
                TestItem loginB = new TestItem(typeof(Login), new TestItem[] { createServer, createUserB });
                TestItem createTitle = new TestItem(typeof(AddNewTitle), new TestItem[] { rootLogin }, null, new TestItem[] { createUserB });
                TestItem createCopies = new TestItem(typeof(AddItemOfTitle), new TestItem[] { createTitle }, new object[] { 2 });
                TestItem Borrow1 = new TestItem(typeof(BorrowTitle), new TestItem[] { loginA, createTitle }, null, new TestItem[] { createCopies });
                TestItem Borrow2 = new TestItem(typeof(BorrowTitle), new TestItem[] { loginB, createTitle }, null, new TestItem[] { createCopies });
                defs.Add(new TestItem[] { createServer, rootLogin, createUserA, createUserB, loginA, loginB, createTitle, createCopies, Borrow1, Borrow2 });
            }
            {
                TestItem createItems = new TestItem(typeof(AddItemOfTitle), null, new object[] { 1 });
                TestItem borrow = new TestItem(typeof(BorrowTitle), null, null, new TestItem[] { createItems });
                TestItem delay1ExtraDay = new TestItem(
                    typeof(Delay),
                    null,
                    new object[] { TimeSpan.FromMilliseconds((TimeSpan.FromDays(15)).TotalMilliseconds / TimeSkipMultiplier) },
                    new TestItem[] { borrow });

                TestItem fine = new TestItem(typeof(CheckFine), null, new object[] { 1 }, new TestItem[] { delay1ExtraDay });
                defs.Add(new TestItem[] { createItems, borrow, delay1ExtraDay, fine });
            }

            foreach (var sketch in defs)
            {
                var session = new Scenario(sketch).CreateRoadMap().createSessionWithRandomParameters();
                var run = session.Run();
                try
                {
                    run.Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(run.Exception);
                    break;
                }
                finally
                {
                    session.Dispose();
                }
                foreach (var time in session.getRunTimes())
                    Console.WriteLine(time.Key.ToString() + ": " + time.Value.ToString());
                
            }
        }
    }
}
