﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp
{
    public class Title : IEquatable<Title>
    {

        public class Item
        {
            public Title Title { get; private set; }
            public DateTime BorrowDate { get; private set; }
            public User Borrower { get; private set; }
            public bool Borrowed { get { return Borrower != null; } }

            public Item(Title title)
            {
                Title = title;
                Borrower = null;
            }

            public void Borrow(User user)
            {
                if (Borrower != null)
                    throw new InvalidOperationException("Item already borrowed");
                BorrowDate = TestTimeUtil.Now;
                Borrower = user;
            }

            public void Return()
            {
                Borrower = null;
            }
        }

        public string ISBN { get; private set; }

        private List<Item> Items;

        public Title(string isbn)
        {
            ISBN = isbn;
            Items = new List<Item>();
        }

        public void AddCopyToInventory(int ItemCount)
        {
#if THREAD_LOCK
            lock(Items)
            {
#endif
            for (int i = 0 ; i < ItemCount; i++)
                Items.Add(new Item(this));
#if THREAD_LOCK
            }
#endif
        }

        public bool ItemsAvailableToBorrow {
            get
            {
#if THREAD_LOCK
            lock(Items)
            {
#endif
                return Items.Any(i => i.Borrowed == false);
#if THREAD_LOCK
            }
#endif
            }
        }

        public int ItemsAvailable {
            get
            {
#if THREAD_LOCK
            lock(Items)
            {
#endif
                return Items.Count;
#if THREAD_LOCK
            }
#endif
            }
        }

        internal Borrow BorrowCopy(User user)
        {
#if THREAD_LOCK
            lock(Items)
            {
#endif
            for (var i = 0; i < Items.Count; i++)
                if (!Items[i].Borrowed)
                {
                    Items[i].Borrow(user);
                    return new Borrow(Items[i]);
                }
            return null;
#if THREAD_LOCK
            }
#endif
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Title);
        }

        public bool Equals(Title other)
        {
            return other != null &&
                   ISBN == other.ISBN;
        }

        public override int GetHashCode()
        {
            return ISBN.GetHashCode();
        }
    }
}
