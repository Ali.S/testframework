﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryApp
{
    public class User : IEquatable<User>
    {
        public string UserId { get; private set; }
        internal List<Borrow> Loans;
        public int TotalFine
        {
            get
            {
#if THREAD_LOCK
                lock (Loans)
                {
#endif
                    return Loans.Sum(l => l.Fine);
#if THREAD_LOCK
                }
#endif
            }
        }

        public User(string userId)
        {
            UserId = userId;
            Loans = new List<Borrow>();
        }

        public void Borrow(Title title)
        {
#if THREAD_LOCK
            lock(Loans)
            {
#endif
            if (TotalFine > 0)
                throw new InvalidOperationException("User has not paid their fine");
            if (Loans.Count > 2)
                throw new InvalidOperationException("User borrowed too many books");
            if (!title.ItemsAvailableToBorrow)
                throw new InvalidOperationException("No copies available to borrow");
            if (Loans.Any(L => L.Borrowed.Title == title))
                throw new InvalidOperationException("Another item of requested title already borrowed");
            Loans.Add(title.BorrowCopy(this));
#if THREAD_LOCK
            }
#endif
        }

        public void RenewBorrow(Title title)
        {
#if THREAD_LOCK
            lock(Loans)
            {
#endif
            for (var i = 0; i < Loans.Count; i++)
                if (title.Equals(Loans[i].Borrowed.Title))
                {
                    Loans[i].Renew();
                    return;
                }
            throw new InvalidOperationException("Title was not borrowed");
#if THREAD_LOCK
            }
#endif
        }

        public void Return(Title title)
        {
#if THREAD_LOCK
            lock(Loans)
            {
#endif
            for (var i = 0; i < Loans.Count; i++)
                if (title.Equals(Loans[i].Borrowed.Title))
                    {
                        Loans[i].Borrowed.Return();
                        Loans.RemoveAt(i);
                        return;
                    }
            throw new InvalidOperationException("Title was not borrowed");
#if THREAD_LOCK
            }
#endif
        }

        public IEnumerable<Title> BorrowedTitles
        {
            get
            {
#if THREAD_LOCK
            lock(Loans)
            {
#endif
                return Loans.Select(l => l.Borrowed.Title);
#if THREAD_LOCK
            }
#endif
            }
        }

        public bool Equals(User other)
        {
            return other != null && other.UserId == UserId;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as User);
        }

        public override int GetHashCode()
        {
            return UserId.GetHashCode();
        }

        public override string ToString()
        {
            return UserId.ToString();
        }
    }
}
