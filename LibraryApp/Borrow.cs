﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp
{
    public class Borrow
    {
        public readonly static int DelayCost = 1;
        public readonly static TimeSpan AllowedLoanTime = TimeSpan.FromDays(14);
        public Title.Item Borrowed { get; private set; }
        public int RenewCount { get; private set; }
        public TimeSpan Delay
        {
            get
            {
                var now = TestTimeUtil.Now;
                var delay = now - Borrowed.BorrowDate;
                delay -= TimeSpan.FromMilliseconds(AllowedLoanTime.TotalMilliseconds * (RenewCount + 1));
                if (delay < TimeSpan.FromSeconds(0))
                    delay = TimeSpan.FromSeconds(0);
                return delay;
            }
        }
        public int Fine
        {
            get
            {
                return Delay.Days * DelayCost;
            }
        }

        public Borrow(Title.Item item)
        {
            Borrowed = item;
            RenewCount = 0;
        }
        
        
        public void Renew()
        {
#if THREAD_LOCK
            lock(this)
            {
#endif
            if (Delay != TimeSpan.FromSeconds(0))
                throw new InvalidOperationException("User can not renew with delay");
            if (RenewCount < 3)
                throw new InvalidOperationException("Borrow was already renewed three times");
            else
                RenewCount++;
#if THREAD_LOCK
            }
#endif
        }
    }
}
