﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.IO;
using System.Threading.Tasks;

namespace LibraryApp {
    public class Library
    {
        protected Dictionary<string, User> Users { get; private set; }
        protected Dictionary<string, Title> Titles { get; private set; }

        TcpListener listener;
        Dictionary<TcpClient, User> clients;


        public Library()
        {
            Users = new Dictionary<string, User>();
            Titles = new Dictionary<string, Title>();
        }

        internal void startServer(int port)
        {
            try
            {
                addUser("root");
            }
            catch (Exception)
            {
            }
            clients = new Dictionary<TcpClient, User>();
            listener = new TcpListener(System.Net.IPAddress.Any, port);
            listener.Start();
            Action<Task<TcpClient>> clientConnected = null;
            clientConnected = (t) =>
            {
                if (t.IsCompleted)
                {
                    setupClient(t.Result);
                    listener.AcceptTcpClientAsync().ContinueWith(clientConnected);
                }
            };
            listener.AcceptTcpClientAsync().ContinueWith(clientConnected);
        }

        private void setupClient(TcpClient client)
        {
            clients.Add(client, null);
            StreamWriter wr = new StreamWriter(client.GetStream());
            StreamReader sr = new StreamReader(client.GetStream());
            Action<Task<string>> messageRecieved = null;
            messageRecieved = (t) =>
            {
                if (t.IsCompleted)
                {
                    try
                    {
                        handleRequest(client, t.Result);
                        wr.WriteLine("?");
                        wr.Flush();
                        sr.ReadLineAsync().ContinueWith(messageRecieved);
                    }
                    catch (Exception)
                    {
                        client.Close();
                        clients.Remove(client);
                    }

                }
            };
            try
            {
                wr.WriteLine("?");
                wr.Flush();
                sr.ReadLineAsync().ContinueWith(messageRecieved);
            }
            catch (Exception)
            {
                client.Close();
                clients.Remove(client);
            }
        }



        internal int getClientCount()
        {
            return clients.Count;
        }

        private void handleRequest(TcpClient c, string request)
        {
            var parts = request.ToLower().Split(' ');
            var outStream = new StreamWriter(c.GetStream());
            try
            {
                switch (parts[0])
                {
                    case "login":
                        if (clients[c] != null)
                            throw new InvalidOperationException("login request is invalid");
                        string userId = parts[1];
                        if (!Users.ContainsKey(userId))
                            throw new InvalidOperationException("user does not exist");
                        clients[c] = Users[userId];
                        outStream.WriteLine("welcome " + userId);
                        break;
                    case "create":
                        if (clients[c]?.UserId != "root")
                            throw new InvalidOperationException("insufficient privilages");
                        switch (parts[1])
                        {
                            case "user":
                                addUser(parts[2]);
                                break;
                            case "title":
                                addTitle(parts[2]);
                                break;
                            case "items":
                                if (!Titles.ContainsKey(parts[2]))
                                    throw new InvalidOperationException("title does not exist");
                                getTitle(parts[2]).AddCopyToInventory(int.Parse(parts[3]));
                                break;
                        }
                        outStream.WriteLine("done");
                        break;
                    case "borrow":
                        {
                            if (clients[c] == null)
                                throw new InvalidOperationException("user not logged in");
                            var title = getTitle(parts[1]);
                            if (title == null)
                                throw new InvalidOperationException("title does not exist");
                            clients[c].Borrow(title);
                            outStream.WriteLine("done");
                        }
                        break;
                    case "renew":
                        {
                            if (clients[c] == null)
                                throw new InvalidOperationException("user not logged in");
                            var title = getTitle(parts[1]);
                            if (title == null)
                                throw new InvalidOperationException("title does not exist");
                            clients[c].RenewBorrow(title);
                            outStream.WriteLine("done");
                        }
                        break;
                    case "return":
                        {
                            if (clients[c] == null)
                                throw new InvalidOperationException("user not logged in");
                            var title = getTitle(parts[1]);
                            if (title == null)
                                throw new InvalidOperationException("title does not exist");
                            clients[c].Return(title);
                            outStream.WriteLine("done");
                        }
                        break;
                    case "list":
                        {
                            switch (parts[1])
                            {
                                case "titles":
#if THREAD_LOCK
                                    lock (Titles)
                                    {
#endif
                                        foreach (var i in Titles)
                                        {
                                            if (i.Value.ItemsAvailableToBorrow)
                                                outStream.WriteLine(i.Value.ISBN + " -> " + i.Value.ItemsAvailable + " items registered");
                                            else
                                                outStream.WriteLine(i.Value.ISBN + " -> " + i.Value.ItemsAvailable + " items registered, not any available to borrow" );
                                        }
#if THREAD_LOCK
                                    }
#endif
                                    break;
                                case "users":
                                    if (clients[c]?.UserId == "insufficient privilages")
                                        throw new InvalidOperationException("user not logged in");
#if THREAD_LOCK
                                    lock (Users)
                                    {
#endif
                                        foreach (var i in Users)
                                        {
                                            var fine = i.Value.TotalFine;
                                            outStream.WriteLine(i.Value.UserId + " -> " + fine + " total fine");
                                        }
#if THREAD_LOCK
                                    }
#endif
                                    break;
                                case "borrows":
                                    if (clients[c] == null)
                                        throw new InvalidOperationException("user not logged in");
                                    foreach (var t in clients[c].Loans)
                                    {
                                        var fine = t.Fine;
                                        if (fine == 0)
                                            outStream.WriteLine(t.Borrowed.Title.ISBN + " -> no fine until " + (t.Borrowed.BorrowDate + Borrow.AllowedLoanTime).ToShortDateString());
                                        else
                                            outStream.WriteLine(t.Borrowed.Title.ISBN + " delayed since " + (t.Borrowed.BorrowDate + Borrow.AllowedLoanTime).ToShortDateString() + " -> fine = " + fine.ToString());
                                    }
                                    outStream.WriteLine("Total fine = " + clients[c].TotalFine);
                                    break;
                            }
                            break;
                        }
                    case "set_time_scale":
                        TestTimeUtil.TimeSkipMultiplier = double.Parse(parts[1]);
                        break;
                    case "help":
                    case "?":
                    case "commands":
                    default:
                        outStream.WriteLine("command list:");
                        outStream.WriteLine("login [userid]");
                        if (clients[c]?.UserId == "root")
                            outStream.WriteLine("create title [isbn]");
                        if (clients[c]?.UserId == "root")
                            outStream.WriteLine("create user [userId]");
                        if (clients[c]?.UserId == "root")
                            outStream.WriteLine("create items [isbn] [number]");
                        if (clients[c]?.UserId == "root")
                            outStream.WriteLine("set_time_scale [scale]");
                        outStream.WriteLine("borrow [title]");
                        outStream.WriteLine("renew [title]");
                        outStream.WriteLine("return [title]");
                        outStream.WriteLine("list titles");
                        outStream.WriteLine("list borrows");
                        break;
                }
            }
            catch (Exception e)
            {
                outStream.WriteLine(e.ToString());
            }
            finally
            {
                outStream.Flush();
            }
        }



        public void addUser(string userId)
        {
#if THREAD_LOCK
            lock (Users)
            {
#endif
                if (Users.ContainsKey(userId))
                    throw new InvalidOperationException("Error: Duplicate User");
                Users.Add(userId, new User(userId));
#if THREAD_LOCK
            }
#endif
        }

        public bool userExists(string userId)
        {
#if THREAD_LOCK
            lock (Users)
            {
#endif
            return Users.ContainsKey(userId);
#if THREAD_LOCK
            }
#endif
        }

        public void addTitle(string isbn)
        {
#if THREAD_LOCK
            lock (Titles)
            {
#endif
            if (Titles.ContainsKey(isbn))
                throw new InvalidOperationException("Error: Duplicate Title");
            Titles.Add(isbn, new Title(isbn));
#if THREAD_LOCK
            }
#endif
        }

        public bool titleExists(string userId)
        {
#if THREAD_LOCK
            lock (Titles)
            {
#endif
            return Titles.ContainsKey(userId);
#if THREAD_LOCK
            }
#endif
        }


            public User getUser(string userId)
        {
#if THREAD_LOCK
            lock (Users)
            {
#endif
            if (Users.ContainsKey(userId))
                return Users[userId];
            else
                return null;
#if THREAD_LOCK
            }
#endif
        }

        public Title getTitle(string isbn)
        {
#if THREAD_LOCK
            lock (Titles)
            {
#endif
            if (Titles.ContainsKey(isbn))
                return Titles[isbn];
            else
                return null;
#if THREAD_LOCK
            }
#endif
        }
    }
}