﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace LibraryApp
{
    public static class TestTimeUtil
    {
        public static double TimeSkipMultiplier = 1;
        public static DateTime Now{
            get
            {
                var diff = DateTime.Now - Process.GetCurrentProcess().StartTime;
                diff = TimeSpan.FromMilliseconds(diff.TotalMilliseconds * TimeSkipMultiplier);
                return Process.GetCurrentProcess().StartTime + diff;
            }
        }
    }
}
