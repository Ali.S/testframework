﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace LibraryApp
{
    class Program
    {
        static void Main(string[] args)
        {
            const int connectionPort = 25613;
            string line = " ";
            while (line.ToLower()[0] != 's' && line.ToLower()[0] != 'c')
            {
                Console.WriteLine("(S)erver or (C)lient?");
                line = Console.ReadLine();
            }
            if (line.ToLower()[0] == 's')
            {
                Library library = new Library();
                library.startServer(connectionPort);
                while (true)
                {
                    Console.WriteLine("clients: " + library.getClientCount().ToString());
                    System.Threading.Thread.Sleep(1000);
                }
            }

            Console.WriteLine("Server Address?");
            line = Console.ReadLine();
            TcpClient client = new TcpClient(line, connectionPort);
            if (!client.Connected)
            {
                Console.WriteLine("Server not available");
                return;
            }

            StreamReader sr = new StreamReader(client.GetStream());
            StreamWriter rw = new StreamWriter(client.GetStream());

            Action<Task<string>> messageRecieved = null;
            bool connected = true;
            messageRecieved = (t) =>
            {
                if (t.IsCompleted)
                {
                    try
                    {
                        if (t.Result.Length == 0)
                        {
                            connected = false;
                        }
                        else
                        {
                            Console.WriteLine(t.Result);
                            sr.ReadLineAsync().ContinueWith(messageRecieved);
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Server disconnected.");
                        connected = false;
                    }
                }
            };
            sr.ReadLineAsync().ContinueWith(messageRecieved);
            while (connected)
            {
                var command = System.Console.ReadLine();
                try
                {
                    rw.WriteLine(command);
                    rw.Flush();
                }
                catch (Exception)
                {
                    Console.WriteLine("Server disconnected.");
                    connected = false;
                }
            }
        }
    }
}
